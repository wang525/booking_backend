const https = require('https');
const axios = require('axios');

var appRouter = function(app) {

    var zeusInstance = axios.create( {
        baseURL:"https://test-api-v3.zumata.com/",
        headers: {
            'Content-Type': 'application/json',
                'X-Api-Key':'5042ae43-516c-4649-9f1a-79e5462628c5',
                'X-Session': 'bf1882b665bf480caec3f99ad32b77a7v'
        }
    })

    var athenaInstance = axios.create( {
        baseURL:"http://data.zumata.com/",
        headers: {
            'Content-Type': 'application/json',
                // 'X-Api-Key':'5042ae43-516c-4649-9f1a-79e5462628c5',
                // 'X-Session': 'bf1882b665bf480caec3f99ad32b77a7v'
        }
    })

    app.post("/hotel-list", function(req, res) {
        var searchData = req.body;
        var hotels;
        console.log(req.body);
        
        athenaInstance.get('/region/'+searchData.region+'/zumata_hotels')
        .then(response => {
                
                hotels = [];
                response.data && response.data.map((hotel, index) => {
                    if (index < 200) {
                        hotels.push(hotel)
                    }
                })
                
                var data = {
                    hotel_list: hotels
                }
                athenaInstance.post('/hotels/en_US/long.json', data)
                .then(response => {
                    res.send(response.data);
                });
            })
            .catch(error => {
                console.log(error);
            });
    });

    app.post("/hotel-detail", function(req, res) {
        // single property
        var paramsObject = req.body;
        // only for test...
        paramsObject['hotel_id'] = 'PLPo';

        var queryParam = '/hotel_rooms?hotel_id=' + paramsObject.hotel_id;
        queryParam += '&check_in_date=' + paramsObject.check_in_date;
        queryParam += '&check_out_date=' + paramsObject.check_out_date;
        queryParam += '&room_count=' + paramsObject.room_count;
        queryParam += '&adult_count=' + paramsObject.adult_count;
        queryParam += '&currency=USD&source_market=US';

        zeusInstance.get(queryParam)
        .then(response => {
            athenaInstance.get('/hotels/'+ paramsObject.hotel_id + '/en_US/long.json')
            .then(respond => {
                var data = {
                    hotel_detail: respond.data,
                    room_detail:response.data
                }
                res.send(data)
            });
        });

        // multiple property
        // now there is not data for shanghai so going to use static data. 
        // hotels = ["kLxE", "BlKz", "8Ycj", "PLPo", "K5Rm", "09uu", "dnhy", "XNVf", "9CPH", "NBUd", "7uLH", "fst1", "fst2", "fst4"];
        // hotels = hotels.toString();
        
        // var paramsObject = {
        //     hotel_id_list: hotels,
        //     check_in_date: req.body.check_in_date,
        //     check_out_date: req.body.check_out_date,
        //     adult_count: req.body.adult_count,
        //     room_count: req.body.room_count,
        //     // children: 0, // searchData.children,
        //     currency: 'USD',
        //     source_market: 'US'
        // }
        
        // zeusInstance.get('/hotel_list', {
        //     params: paramsObject
        // })
        // .then(response => {
        //         console.log('hotel lists ===', response.data);
        //         res.send(response.data);
        //     })
        //     .catch(error => {
        //         console.log(error);
        //     });
    });

    app.post("/autosuggest", function(req, res) {
        var searchTerm = req.body;

        athenaInstance.get('/autosuggest?term=' + searchTerm.term + '&types[]=city_en_us')
        .then(response => {
            res.send(response.data);
        });
    });

    app.post("/bookpolicy", function(req, res) {
        var queryParam = req.body;

        zeusInstance.post('/booking_policy', queryParam)
        .then(response => {
            // console.log('booking policy', response.data)
            res.send(response.data);
        });
    });

    app.post("/prebook", function(req, res) {
        var queryParam = req.body;

        zeusInstance.post('/pre_book', queryParam)
        .then(response => {
            bookingData = response.data;

            if (bookingData && bookingData.booking_id) {

                zeusInstance.post('/book/'+ bookingData.booking_id)
                .then(respond => {

                    if (respond.data) {
                        zeusInstance.get('/book/'+ bookingData.booking_id + '/status')
                        .then(resp => {

                            if (resp.data) {
                                res.send(resp.data);
                            }
                        });
                    }

                });
            }

        });
    });

    app.post("/cancelbook", function(req, res) {
        var queryParam = req.body;

        zeusInstance.post('/cancel', queryParam)
        .then(response => {
            // console.log('booking policy', response.data)
            res.send(response.data);
        });
    });
}

module.exports = appRouter;